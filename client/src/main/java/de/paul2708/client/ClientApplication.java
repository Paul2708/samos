package de.paul2708.client;

import de.paul2708.client.rest.RestClient;
import de.paul2708.client.window.LoginWindow;
import javafx.application.Application;
import javafx.stage.Stage;

/**
 * Created by Paul on 02.10.2017.
 */
public class ClientApplication extends Application {

    private static ClientApplication instance;

    private LoginWindow loginWindow;

    private RestClient client;

    public ClientApplication() {
        ClientApplication.instance = this;

        this.client = new RestClient();
    }

    @Override
    public void start(Stage stage) throws Exception {
        this.loginWindow = new LoginWindow(stage);

        this.loginWindow.showWindow();
    }

    public LoginWindow getLoginWindow() {
        return loginWindow;
    }

    public RestClient getClient() {
        return client;
    }

    public static ClientApplication getInstance() {
        return instance;
    }
}
