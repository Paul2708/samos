package de.paul2708.client;

import javafx.application.Application;

/**
 * Created by Paul on 02.10.2017.
 */
public class Main {

    public static void main(String[] args) {
        ClientApplication application = new ClientApplication();

        Application.launch(application.getClass(), args);
    }
}
