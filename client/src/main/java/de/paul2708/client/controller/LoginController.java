package de.paul2708.client.controller;

import de.paul2708.client.ClientApplication;
import de.paul2708.common.response.DetailedResponse;
import de.paul2708.common.user.UserInfo;
import de.paul2708.common.util.Util;
import de.paul2708.common.verify.PasswordVerifier;
import de.paul2708.common.verify.UserNameVerifier;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.paint.Color;

import java.net.URL;
import java.util.ResourceBundle;

/**
 * Created by Paul on 02.10.2017.
 */
public class LoginController implements Initializable {

    @FXML
    private TextField userField;
    @FXML
    private PasswordField passwordField;
    @FXML
    private Label errorLabel;

    private UserNameVerifier userNameVerifier;
    private PasswordVerifier passwordVerifier;

    public LoginController() {
        this.userNameVerifier = new UserNameVerifier();
        this.passwordVerifier = new PasswordVerifier();
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        // TODO: Paste config values
    }

    @FXML
    private void buttonPressed() {
        errorLabel.setText("");
        errorLabel.setTextFill(Color.RED);

        String userName = userField.getText().trim();
        String password = passwordField.getText().trim();

        // Check user and password
        String userError = userNameVerifier.verify(userName);
        String passwordError = passwordVerifier.verify(password);

        if (userError == null && passwordError == null) {
            UserInfo info = new UserInfo();
            info.setName(userName);
            info.setPassword(Util.createHash(password));

            ClientApplication.getInstance().getClient().login(info, response -> {
                Platform.runLater(() -> {
                    if (response.equals(DetailedResponse.LIMIT_REACHED)) {
                        errorLabel.setText("Du hast zu viele Anfragen an den Server geschickt.");
                    } else if (response.equals(DetailedResponse.LOGIN_FAILED)) {
                        errorLabel.setText("Der Benutzername und Passwort stimmen nicht überein.");
                    } else if (response.equals(DetailedResponse.LOGIN_DONE)) {
                        errorLabel.setTextFill(Color.GREEN);
                        errorLabel.setText("Du hast dich erfolgreich angemeldet.");
                    }
                });
            });
        } else {
            errorLabel.setText(userError != null ? userError : passwordError);
        }
    }

    @FXML
    private void linkToMissingPassword() {
        // TODO: Open dialog
        System.out.println("missing pw");
    }

    @FXML
    private void linkToRegistration() {
        ClientApplication.getInstance().getLoginWindow().showRegistrationDialog();
    }
}
