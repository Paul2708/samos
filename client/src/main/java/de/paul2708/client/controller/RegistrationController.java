package de.paul2708.client.controller;

import de.paul2708.client.ClientApplication;
import de.paul2708.common.response.DetailedResponse;
import de.paul2708.common.user.UserInfo;
import de.paul2708.common.util.Util;
import de.paul2708.common.verify.EMailVerifier;
import de.paul2708.common.verify.PasswordVerifier;
import de.paul2708.common.verify.UserNameVerifier;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.paint.Color;

/**
 * Created by Paul on 02.10.2017.
 */
public class RegistrationController {

    @FXML
    private TextField userField;
    @FXML
    private PasswordField passwordField;
    @FXML
    private PasswordField secondPasswordField;
    @FXML
    private TextField eMailField;
    @FXML
    private DatePicker datePicker;
    @FXML
    private Label errorLabel;

    private UserNameVerifier userNameVerifier;
    private PasswordVerifier passwordVerifier;
    private EMailVerifier eMailVerifier;

    public RegistrationController() {
        this.userNameVerifier = new UserNameVerifier();
        this.passwordVerifier = new PasswordVerifier();
        this.eMailVerifier = new EMailVerifier();
    }

    @FXML
    private void buttonPressed() {
        errorLabel.setText("");
        errorLabel.setTextFill(Color.RED);

        // Check information
        if (userNameVerifier.verify(userField.getText()) != null) {
            errorLabel.setText(userNameVerifier.verify(userField.getText()));
        } else if (passwordVerifier.verify(passwordField.getText()) != null) {
            errorLabel.setText(passwordVerifier.verify(passwordField.getText()));
        } else if (!passwordField.getText().equals(secondPasswordField.getText())) {
            errorLabel.setText("Die Passwörter stimmen nicht überein.");
        } else if (!eMailField.getText().trim().equals("") && eMailVerifier.verify(eMailField.getText()) != null) {
            errorLabel.setText(eMailVerifier.verify(eMailField.getText()));
        } else if (datePicker.getValue() == null) {
            errorLabel.setText("Das Geburtsdatum ist ungültig.");
        } else {
            register();
        }
    }

    private void register() {
        UserInfo info = new UserInfo(userField.getText().trim(),
                Util.createHash(passwordField.getText()),
                eMailField.getText().trim(),
                datePicker.getValue().toString());

        ClientApplication.getInstance().getClient().register(info, response ->
                Platform.runLater(() -> {
                    if (response.equals(DetailedResponse.LIMIT_REACHED)) {
                        errorLabel.setText("Du hast zu viele Anfragen an den Server geschickt.");
                    } else if (response.equals(DetailedResponse.USERNAME_USED)) {
                        errorLabel.setText("Der Benutzername ist bereits in Verwendung.");
                    } else if (response.equals(DetailedResponse.REGISTRATION_DONE)) {
                        errorLabel.setTextFill(Color.GREEN);
                        errorLabel.setText("Du hast dich erfolgreich registriert. Melde dich nun an.");
                    }

                    clearNodes();
                })
        );
    }

    private void clearNodes() {
        userField.clear();
        passwordField.clear();
        secondPasswordField.clear();
        eMailField.clear();
        datePicker.setValue(null);
    }
}
