package de.paul2708.client.rest;

import de.paul2708.common.response.DetailedResponse;
import de.paul2708.common.user.UserInfo;
import de.paul2708.common.util.Callback;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * Created by Paul on 06.10.2017.
 */
public class RestClient {

    private Client client;

    private WebTarget registerTarget;
    private WebTarget loginTarget;

    public RestClient() {
        this.client = ClientBuilder.newClient();

        this.registerTarget = client.target("http://localhost:8080/webapi/registration");
        this.loginTarget = client.target("http://localhost:8080/webapi/login");
    }

    public void register(UserInfo info, Callback<DetailedResponse> callback) {
        new Thread(() -> {
            Response response = registerTarget.request(MediaType.APPLICATION_JSON).post(Entity.json(info));
            DetailedResponse detailedResponse = response.readEntity(DetailedResponse.class);

            callback.accpet(detailedResponse);
        }).start();
    }

    public void login(UserInfo info, Callback<DetailedResponse> callback) {
        new Thread(() -> {
            Response response = loginTarget.request(MediaType.APPLICATION_JSON).post(Entity.json(info));
            DetailedResponse detailedResponse = response.readEntity(DetailedResponse.class);

            callback.accpet(detailedResponse);
        }).start();
    }
}
