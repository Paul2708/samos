package de.paul2708.client.storage;

import java.io.InputStream;
import java.util.Properties;

/**
 * Created by Paul on 03.10.2017.
 */
public class Config {

    // TODO: Implement config

    private Properties properties;

    public Config() {
        this.properties = new Properties();
    }

    public Config load() {
        try {
            ClassLoader classLoader = ClassLoader.getSystemClassLoader();
            InputStream inputStream =  classLoader.getResourceAsStream("storage/config.properties");
            properties.load(inputStream);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return this;
    }

    public void save() {
        // TODO: Save properties
    }

    public void setUserName(String userName) {
        properties.setProperty("username", userName);
    }

    public void setPassword(String password) {
        properties.setProperty("password", password);
    }

    public String getUserName() {
        return properties.getProperty("username", "null");
    }

    public String getPassword() {
        return properties.getProperty("password", "null");
    }
}
