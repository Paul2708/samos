package de.paul2708.client.window;

import de.paul2708.common.util.Util;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.layout.Pane;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.io.IOException;

/**
 * Created by Paul on 02.10.2017.
 */
public class LoginWindow {

    private Stage stage;

    private Scene loginScene;

    public LoginWindow(Stage stage) throws IOException {
        this.stage = stage;

        setup();
    }

    private void setup() throws IOException {
        // Stage
        stage.getIcons().add(new Image("logo/logo.png"));
        stage.setResizable(false);

        // Login scene
        Pane root = FXMLLoader.load(Util.getResource("view/LoginView.fxml"));
        this.loginScene = new Scene(root);
    }

    public void showWindow() {
        stage.setTitle("Samos - Anmelden");

        stage.setScene(loginScene);
        stage.show();
    }

    public void showRegistrationDialog() {
        try {
            // Register stage
            Stage registerStage = new Stage();

            registerStage.getIcons().add(new Image("logo/logo.png"));
            registerStage.setResizable(false);

            registerStage.initModality(Modality.WINDOW_MODAL);
            registerStage.initOwner(stage);
            registerStage.setTitle("Samos - Registrieren");

            // Register scene
            Pane root = FXMLLoader.load(Util.getResource("view/RegisterView.fxml"));
            Scene registerScene = new Scene(root);

            registerStage.setScene(registerScene);
            registerStage.showAndWait();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
