package de.paul2708.common.config;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Properties;

/**
 * Created by Paul on 04.10.2017.
 */
public abstract class BasicConfig {

    private String path;
    private String comment;

    private File file;
    public Properties properties;

    public BasicConfig(String path, String comment) {
        this.path = path;
        this.comment = comment;

        this.properties = new Properties();
    }

    public BasicConfig(String path) {
        this.path = path;

        this.properties = new Properties();
    }

    public abstract Properties defaultValue();

    public void load() {
        try {
            Path path = Paths.get(this.path);
            this.file = path.toFile();

            if (!file.exists()) {
                if (path.getParent() != null) {
                    Files.createDirectories(path.getParent());
                }

                Files.createFile(path);
                this.properties = defaultValue();
                save();
            } else {
                properties.load(new FileInputStream(file));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void save() {
        try {
            FileOutputStream outputStream = new FileOutputStream(file);
            properties.store(outputStream, comment);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
