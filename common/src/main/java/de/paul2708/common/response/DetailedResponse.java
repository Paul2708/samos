package de.paul2708.common.response;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 * Created by Paul on 05.10.2017.
 */
@XmlRootElement
public class DetailedResponse {

    private int id;
    private int status;
    private String message;
    private String description;

    public DetailedResponse() { }

    public DetailedResponse(int id, int status, String message, String description) {
        this.id = id;
        this.status = status;
        this.message = message;
        this.description = description;
    }

    public DetailedResponse description(String description) {
        this.description = description;
        return this;
    }

    public Response toResponse() {
        return Response.status(status).type(MediaType.APPLICATION_JSON)
                .entity(this).build();
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getId() {
        return id;
    }

    @XmlTransient
    public int getStatus() {
        return status;
    }

    public String getMessage() {
        return message;
    }

    public String getDescription() {
        return description;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof DetailedResponse) {
            DetailedResponse response = (DetailedResponse) obj;

            if (id == response.getId() && id != -1) {
                return true;
            } else return message.equalsIgnoreCase(response.getMessage());
        }

        return false;
    }

    @Override
    public String toString() {
        return "DetailedResponse{" +
                "id=" + id +
                ", status=" + status +
                ", message='" + message + '\'' +
                ", description='" + description + '\'' +
                '}';
    }

    public static DetailedResponse LIMIT_REACHED = new DetailedResponse.Builder().id(0).status(401).message("Limit reached")
            .description("You are not allowed to send more than 15 requests per minutes.").build();
    public static DetailedResponse USERNAME_USED = new DetailedResponse.Builder().id(1).status(420).message("Username already used")
                    .description("The username is already in use.").build();
    public static DetailedResponse REGISTRATION_DONE = new DetailedResponse.Builder().id(2).status(201).message("Registration done")
                .description("You are registrated.").build();
    public static DetailedResponse LOGIN_FAILED = new DetailedResponse.Builder().id(3).status(401).message("Login failed")
            .description("Username and password doesn't match.").build();
    public static DetailedResponse LOGIN_DONE = new DetailedResponse.Builder().id(4).status(200).message("Login done")
            .description("You are logged in.").build();

    public static class Builder {

        private int id = -1;
        private int status;
        private String message;
        private String description;

        public Builder id(int id) {
            this.id = id;
            return this;
        }

        public Builder status(int status) {
            this.status = status;
            return this;
        }

        public Builder message(String error) {
            this.message = error;
            return this;
        }

        public Builder description(String description) {
            this.description = description;
            return this;
        }

        public DetailedResponse build() {
            return new DetailedResponse(id, status, message, description);
        }
    }
}
