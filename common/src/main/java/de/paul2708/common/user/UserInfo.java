package de.paul2708.common.user;

import javax.xml.bind.annotation.XmlRootElement;
import java.util.UUID;

/**
 * Created by Paul on 06.10.2017.
 */
@XmlRootElement
public class UserInfo {

    private String name;
    private String password;
    private String eMail;
    private String birthDate;

    private UUID uuid;

    public UserInfo() { }

    public UserInfo(String name, String password, String eMail, String birthDate) {
        this.name = name;
        this.password = password;
        this.eMail = eMail;
        this.birthDate = birthDate;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void seteMail(String eMail) {
        this.eMail = eMail;
    }

    public void setBirthDate(String birthDate) {
        this.birthDate = birthDate;
    }

    public void setUuid(UUID uuid) {
        this.uuid = uuid;
    }

    public String getName() {
        return name;
    }

    public String getPassword() {
        return password;
    }

    public String geteMail() {
        return eMail;
    }

    public String getBirthDate() {
        return birthDate;
    }

    public UUID getUuid() {
        return uuid;
    }

    @Override
    public String toString() {
        return "UserInfo{" +
                "name='" + name + '\'' +
                ", password='" + password + '\'' +
                ", eMail='" + eMail + '\'' +
                ", birthDate=" + birthDate +
                ", uuid=" + uuid +
                '}';
    }
}
