package de.paul2708.common.util;

import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;

/**
 * Created by Paul on 02.10.2017.
 */
public class Util {

    public static boolean containsNumber(String input) {
        for (int i = 0; i < input.length(); i++) {
            if (Character.isDigit(input.charAt(i))) {
                return true;
            }
        }

        return false;
    }

    public static boolean containsIllegalChars(String input, char[] array) {
        for (int i = 0; i < input.length(); i++) {
            char letter = input.charAt(i);

            if (!Character.isLetterOrDigit(letter)) {
                for (int j = 0; j < array.length; j++) {
                    if (letter != array[j]) {
                        return true;
                    }
                }
            }
        }

        return false;
    }

    public static URL getResource(String path) {
        return ClassLoader.getSystemClassLoader().getResource(path);
    }

    public static String createHash(String input) {
        try {
            MessageDigest digest = MessageDigest.getInstance("SHA-256");
            byte[] hash = digest.digest(input.getBytes(StandardCharsets.UTF_8));

            return Base64.getEncoder().encodeToString(hash);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            return null;
        }
    }
}
