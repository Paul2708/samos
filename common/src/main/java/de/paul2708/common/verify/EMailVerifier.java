package de.paul2708.common.verify;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Paul on 02.10.2017.
 */
public class EMailVerifier {

    private static final String PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
            + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

    private Pattern pattern;

    public EMailVerifier() {
        this.pattern = Pattern.compile(PATTERN);
    }

    public String verify(String input) {
        input = input.trim();

        Matcher matcher = pattern.matcher(input);

        if (!matcher.matches()) {
            return "Die E-Mail enthält ein ungültiges Format.";
        }

        return null;
    }

}
