package de.paul2708.common.verify;

import de.paul2708.common.util.Util;

/**
 * Created by Paul on 02.10.2017.
 */
public class PasswordVerifier {

    private final static int MIN_LENGTH = 6;
    private final static int MAX_LENGTH = 20;
    private final static boolean ALLOW_NUMBERS = true;
    private final static char[] ALLOWED_CHARS = { '_', '.', '*', '-', '+', ':', '#', '!', '?', '%', '{', '}', '|', '@', '[', ']', ';', '=', '“', '&', '$', '\\', '/', ',', '(', ')' };

    public String verify(String input) {
        input = input.trim();

        if (input.length() < MIN_LENGTH) {
            return "Das Passwort muss mindestens " + MIN_LENGTH + " Zeichen haben.";
        } else if (input.length() > MAX_LENGTH) {
            return "Das Passwort darf höchstens " + MAX_LENGTH + " Zeichen haben.";
        } else if (Util.containsNumber(input) && !ALLOW_NUMBERS) {
            return "Das Passwort darf keine " + MAX_LENGTH + " Zahlen enthalten.";
        } else if (Util.containsIllegalChars(input, ALLOWED_CHARS)) {
            return "Das Passwort darf nur folgende Sonderzeichen enthalten: " + String.copyValueOf(ALLOWED_CHARS);
        } else {
            return null;
        }
    }

}
