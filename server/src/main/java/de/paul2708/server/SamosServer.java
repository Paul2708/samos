package de.paul2708.server;

import de.paul2708.server.config.DatabaseConfig;
import de.paul2708.server.config.WebConfig;
import de.paul2708.server.database.DatabaseManager;
import de.paul2708.server.rest.request.RequestCache;
import de.paul2708.server.rest.RestServer;
import de.paul2708.server.user.UserManager;

/**
 * Created by Paul on 04.10.2017.
 */
public class SamosServer {

    private static SamosServer instance;

    private DatabaseConfig databaseConfig;
    private WebConfig webConfig;

    private DatabaseManager databaseManager;

    private UserManager userManager;

    private RestServer restServer;
    private RequestCache requestCache;

    public SamosServer() {
        SamosServer.instance = this;
    }

    public void startUp() {
        // Config files
        this.databaseConfig = new DatabaseConfig();
        this.databaseConfig.load();

        this.webConfig = new WebConfig();
        this.webConfig.load();

        // Database connection
        this.databaseManager = new DatabaseManager(databaseConfig.getUser(), databaseConfig.getDatabase(),
                databaseConfig.getPassword().toCharArray());
        this.databaseManager.connect();
        this.databaseManager.setup();

        // User manager
        this.userManager = new UserManager();
        this.userManager.loadUsers();

        // RESTful web service
        this.restServer = new RestServer();
        this.restServer.start(webConfig.getPort(), webConfig.getPath());

        this.requestCache = new RequestCache(webConfig.getLimit());
    }

    public DatabaseConfig getDatabaseConfig() {
        return databaseConfig;
    }

    public WebConfig getWebConfig() {
        return webConfig;
    }

    public DatabaseManager getDatabaseManager() {
        return databaseManager;
    }

    public UserManager getUserManager() {
        return userManager;
    }

    public RestServer getRestServer() {
        return restServer;
    }

    public RequestCache getRequestCache() {
        return requestCache;
    }

    public static SamosServer getInstance() {
        return instance;
    }
}
