package de.paul2708.server.config;

import de.paul2708.common.config.BasicConfig;

import java.util.Properties;

/**
 * Created by Paul on 04.10.2017.
 */
public class DatabaseConfig extends BasicConfig {

    public DatabaseConfig() {
        super("config/database.properties", "MongoDB properties");
    }

    @Override
    public Properties defaultValue() {
        Properties properties = new Properties();
        properties.setProperty("user", "admin");
        properties.setProperty("password", "password");
        properties.setProperty("database", "samos");

        return properties;
    }

    public String getUser() {
        return properties.getProperty("user");
    }

    public String getPassword() {
        return properties.getProperty("password");
    }

    public String getDatabase() {
        return properties.getProperty("database");
    }
}
