package de.paul2708.server.config;

import de.paul2708.common.config.BasicConfig;

import java.util.Properties;

/**
 * Created by Paul on 04.10.2017.
 */
public class WebConfig extends BasicConfig {

    public WebConfig() {
        super("config/web.properties", "RESTful web service properties");
    }

    @Override
    public Properties defaultValue() {
        Properties properties = new Properties();
        properties.setProperty("port", "8080");
        properties.setProperty("path", "/webapi/*");
        properties.setProperty("limit_per_minute", "15");

        return properties;
    }

    public int getPort() {
        return Integer.valueOf(properties.getProperty("port"));
    }

    public String getPath() {
        return properties.getProperty("path");
    }

    public int getLimit() {
        return Integer.valueOf(properties.getProperty("limit_per_minute"));
    }
}
