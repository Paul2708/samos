package de.paul2708.server.database;

import com.mongodb.MongoClient;
import com.mongodb.MongoCredential;
import com.mongodb.ServerAddress;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.CreateCollectionOptions;
import de.paul2708.common.user.UserInfo;
import org.bson.Document;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;
import java.util.function.Consumer;

/**
 * Created by Paul on 04.10.2017.
 */
public class DatabaseManager {

    private String user;
    private String databaseName;
    private char[] password;

    private MongoClient client;
    private MongoDatabase database;

    public DatabaseManager(String user, String databaseName, char[] password) {
        this.user = user;
        this.databaseName = databaseName;
        this.password = password;
    }

    public void connect() {
        if (user.equals("") && String.copyValueOf(password).equals("")) {
            this.client = new MongoClient();
        } else {
            MongoCredential credential = MongoCredential.createCredential(user, databaseName, password);
            this.client = new MongoClient(new ServerAddress("localhost", 27017), Arrays.asList(credential));
        }

        this.database = client.getDatabase(databaseName);
    }

    public void setup() {
        boolean collectionExists = database.listCollectionNames().into(new ArrayList<>()).contains("user");
        if (!collectionExists) {
            CreateCollectionOptions options = new CreateCollectionOptions().autoIndex(true);
            database.createCollection("user", options);
        }
    }

    public void insertUser(UserInfo info) {
        Document document = new Document("uuid", info.getUuid())
                .append("name", info.getName())
                .append("password", info.getPassword())
                .append("email", info.geteMail())
                .append("birthdate", info.getBirthDate());

        database.getCollection("user").insertOne(document);
    }

    public List<UserInfo> getUsers() {
        List<UserInfo> list = new ArrayList<>();

        database.getCollection("user").find().forEach(new Consumer<Document>() {

            @Override
            public void accept(Document document) {
                UserInfo userInfo = new UserInfo();
                userInfo.setUuid(document.get("uuid", UUID.class));
                userInfo.setName(document.getString("name"));
                userInfo.setPassword(document.getString("password"));
                userInfo.seteMail(document.getString("email"));
                userInfo.setBirthDate(document.getString("birthdate"));

                list.add(userInfo);
            }
        });

        return list;
    }
}
