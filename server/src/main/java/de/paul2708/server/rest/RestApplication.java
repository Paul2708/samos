package de.paul2708.server.rest;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

/**
 * Created by Paul on 03.10.2017.
 */
@ApplicationPath("webapi")
public class RestApplication extends Application {

}
