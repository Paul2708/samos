package de.paul2708.server.rest.filter;

import de.paul2708.common.response.DetailedResponse;
import de.paul2708.server.SamosServer;
import de.paul2708.server.rest.request.RequestCache;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.container.PreMatching;
import javax.ws.rs.core.Context;
import javax.ws.rs.ext.Provider;
import java.io.IOException;

/**
 * Created by Paul on 05.10.2017.
 */
@Provider
@PreMatching
public class LimitFilter implements ContainerRequestFilter {

    @Context
    private HttpServletRequest request;

    @Override
    public void filter(ContainerRequestContext context) throws IOException {
        RequestCache cache = SamosServer.getInstance().getRequestCache();
        String ip = request.getRemoteAddr();

        if (cache.hasReachedLimit(ip)) {
            context.abortWith(DetailedResponse.LIMIT_REACHED.toResponse());
            return;
        }

        cache.addRequest(ip);
    }
}
