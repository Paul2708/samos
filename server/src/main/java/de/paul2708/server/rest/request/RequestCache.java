package de.paul2708.server.rest.request;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;

/**
 * Created by Paul on 05.10.2017.
 */
public class RequestCache {

    private int limit;

    private Map<String, RequestInfo> requests;

    public RequestCache(int limit) {
        this.limit = limit;

        this.requests = new ConcurrentHashMap<>();
    }

    public void addRequest(String ip) {
        if (requests.get(ip) == null) {
            RequestInfo info = new RequestInfo();
            info.setRequests(1);
            info.setLastRequest(System.currentTimeMillis());
            requests.put(ip, info);
        } else {
            RequestInfo info = requests.get(ip);
            info.setRequests(info.getRequests() + 1);
            info.setLastRequest(System.currentTimeMillis());
            requests.put(ip, info);
        }
    }

    public boolean hasReachedLimit(String ip) {
        if (requests.get(ip) != null) {
            RequestInfo info = requests.get(ip);

            if (info.getRequests() >= limit) {
                if (info.getLastRequest() + TimeUnit.MINUTES.toMillis(1) > System.currentTimeMillis()) {
                    return true;
                } else {
                    requests.remove(ip);
                    return false;
                }
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    public int getRequests(String ip) {
        if (requests.get(ip) != null) {
            return requests.get(ip).getRequests();
        } else {
            return 0;
        }
    }
}
