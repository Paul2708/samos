package de.paul2708.server.rest.request;

/**
 * Created by Paul on 06.10.2017.
 */
public class RequestInfo {

    private int requests;
    private long lastRequest;

    public void setRequests(int requests) {
        this.requests = requests;
    }

    public void setLastRequest(long lastRequest) {
        this.lastRequest = lastRequest;
    }

    public int getRequests() {
        return requests;
    }

    public long getLastRequest() {
        return lastRequest;
    }
}
