package de.paul2708.server.rest.resource;

import de.paul2708.common.response.DetailedResponse;
import de.paul2708.common.user.UserInfo;
import de.paul2708.server.SamosServer;
import de.paul2708.server.user.UserManager;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * Created by Paul on 03.10.2017.
 */
@Path("/login")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class LoginResource {

    @POST
    public Response login(UserInfo info) {
        if (info.getName() != null && info.getPassword() != null) {
            UserManager manager = SamosServer.getInstance().getUserManager();
            if (!manager.exists(info.getName()) || !manager.isSamePassword(info.getName(), info.getPassword())) {
                return DetailedResponse.LOGIN_FAILED.toResponse();
            }

            return DetailedResponse.LOGIN_DONE.toResponse();
        }

        return DetailedResponse.LOGIN_FAILED.toResponse();
    }
}
