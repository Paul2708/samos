package de.paul2708.server.rest.resource;

import de.paul2708.common.response.DetailedResponse;
import de.paul2708.common.user.UserInfo;
import de.paul2708.server.SamosServer;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * Created by Paul on 03.10.2017.
 */
@Path("/registration")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class RegistrationResource {

    @POST
    public Response register(UserInfo info) {
        if (SamosServer.getInstance().getUserManager().exists(info.getName())) {
            return DetailedResponse.USERNAME_USED.description("The username '" + info.getName() + "' is already in use.").toResponse();
        }

        SamosServer.getInstance().getUserManager().registerUser(info);
        return DetailedResponse.REGISTRATION_DONE.toResponse();
    }
}
