package de.paul2708.server.user;

import de.paul2708.common.user.UserInfo;
import de.paul2708.server.SamosServer;

import java.util.List;
import java.util.UUID;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * Created by Paul on 06.10.2017.
 */
public class UserManager {

    private List<UserInfo> users;

    public UserManager() {
        this.users = new CopyOnWriteArrayList<>();
    }

    public void loadUsers() {
        this.users = SamosServer.getInstance().getDatabaseManager().getUsers();
    }

    public void registerUser(UserInfo userInfo) {
        userInfo.setUuid(randomUuid());

        this.users.add(userInfo);

        SamosServer.getInstance().getDatabaseManager().insertUser(userInfo);
    }

    public boolean exists(String userName) {
        for (UserInfo user : users) {
            if (user.getName().equalsIgnoreCase(userName)) {
                return true;
            }
        }

        return false;
    }

    public boolean isSamePassword(String name, String password) {
        if (!exists(name)) {
            return false;
        }

        UserInfo info = getInfo(name);
        if (info.getPassword().equals(password)) {
            return true;
        }

        return false;
    }

    private UserInfo getInfo(String name) {
        for (UserInfo user : users) {
            if (user.getName().equalsIgnoreCase(name)) {
                return user;
            }
        }

        return null;
    }

    private UUID randomUuid() {
        UUID uuid = UUID.randomUUID();

        for (UserInfo user : users) {
            if (user.getUuid().equals(uuid)) {
                return randomUuid();
            }
        }

        return uuid;
    }
}
